//
//  TravelPackage.swift
//  arch-training
//
//  Created by Jade Silveira on 27/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import Foundation
import ObjectMapper

class TravelPackage: Mappable {
    
    var title               : String?
    var subtitle            : String?
    var price               : String?
    var image               : String?
    var description         : String?
    var wasBought           : Bool      = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title       <- map["title"]
        subtitle    <- map["subtitle"]
        price       <- map["price"]
        image       <- map["image"]
        description <- map["description"]
    }
    
    func getPrice() -> String {
        return getFormatedPrice(value: price)
    }
    
    func getFormatedPrice(value: String?) -> String {
        if let value = value {
            let newValue = value.contains(".") ? value.replacingOccurrences(of: ".", with: ",") :
                !value.contains(",") ? value + ",00" : value
            
            return "R$ " + newValue
        }
        return "PREÇO INDISPONÍVEL"
    }
}
