//
//  TravelPackageInteractor.swift
//  arch-training
//
//  Created by Jade Silveira on 02/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class TravelPackageInteractor: NSObject {
    
    var output      : TravelPackageInteractorOutput?
    
    init(output: TravelPackageInteractorOutput) {
        super.init()
        
        self.output = output
    }
}

extension TravelPackageInteractor: TravelPackageInteractorInput {
    func setTravelPkgBought(travelPackage: TravelPackage) {
        TravelPackageService.shared.setTravelPackageBought(travelPackageParam: travelPackage, wasBought: true)
    }
}
