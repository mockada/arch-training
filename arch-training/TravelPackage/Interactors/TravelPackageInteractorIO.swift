//
//  TravelPackageInteractorIO.swift
//  arch-training
//
//  Created by Jade Silveira on 02/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

protocol TravelPackageInteractorInput {
    func setTravelPkgBought(travelPackage: TravelPackage)
}

protocol TravelPackageInteractorOutput {
    
}
