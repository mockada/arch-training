//
//  TravelPackageViewController.swift
//  arch-training
//
//  Created by Jade Silveira on 28/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class TravelPackageViewController: BaseViewController {
    
    @IBOutlet weak var travelImage                  : UIImageView!
    @IBOutlet weak var travelTitle                  : UILabel!
    @IBOutlet weak var travelSubtitle               : UILabel!
    @IBOutlet weak var price                        : UILabel!
    @IBOutlet weak var travelDescription            : UILabel!
    @IBOutlet weak var pkgAcquired                  : UIView!
    @IBOutlet weak var buyButton                    : UIButton!
    @IBOutlet weak var descriptionBox               : UIView!
    
    @IBOutlet weak var constraintHeightPkgAcquired  : NSLayoutConstraint!
    
    var eventHandler                                : TravelPackageModuleInterface?
    var travelPackage                               = TravelPackage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventHandler = TravelPackagePresenter(userInterface: self)
        eventHandler?.viewDidLoad()
    }
    
    @IBAction func buy() {
        showMessageConfirmPurchase()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func showPkgAcquired(_ show: Bool) {
        if !show {
            self.pkgAcquired.isHidden = true
            changeToBuy()
            return
        }
        eventHandler?.showPkgAcquired(travelPackage: travelPackage)
    }
}

extension TravelPackageViewController: TravelPackageInterface {
    func showMessageConfirmPurchase() {
        let alertView = self.getUIAlertWithTitle("", message: "Estamos quase lá! Deseja comprar este pacote?", positiveButtonTitle: "Sim!", negativeButtonTitle: "Agora não", positiveHandler: { (positiveAction) in
            self.eventHandler?.buyTravelPackage()
            
        }, negativeHandler: nil)
        
        DispatchQueue.main.async {
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func showMessageBought() {
        let alertView = self.getUIAlertControllerWithTitle("Parabéns!", message: "O pacote " + (travelPackage.title ?? "") + " foi adquirido com sucesso.", actionTitle: "OK", handler: { (action) in
            self.showPkgAcquired(true)
        })
        
        DispatchQueue.main.async {
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func setupTravelPackage() {
        travelTitle.text        = travelPackage.title
        travelSubtitle.text     = travelPackage.subtitle
        price.text              = travelPackage.getPrice()
        travelImage.image       = UIImage(named: travelPackage.image ?? "")
        travelDescription.text  = travelPackage.description
    }
    
    func setupDescriptionBox() {
        let borderWidth : CGFloat = 2.0
        
        descriptionBox.layer.borderColor    = UIColor.init(red: 232/255.0, green: 113/255.0, blue: 80/255.0, alpha: 1).cgColor
        descriptionBox.layer.borderWidth    = borderWidth
        descriptionBox.layer.cornerRadius   = 10
    }
    
    func updateTravelPkg() {
        showPkgAcquired(false)
    }
    
    func animatePkgAcquired() {
        constraintHeightPkgAcquired.constant = 66
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (completion) in
            self.pkgAcquired.fadeIn(customStartAlpha: 0)
        }
    }
    
    func changeToCancel() {
        buyButton.titleLabel?.text = "Cancelar"
        buyButton.backgroundColor? = UIColor(red: 195.0/255.0, green: 24.0/255.0, blue: 11.0/255.0, alpha: 1.0)
    }
    
    func changeToBuy() {
        buyButton.titleLabel?.text = "Comprar"
        buyButton.backgroundColor? = UIColor(red: 80.0/255.0, green: 195.0/255.0, blue: 78.0/255.0, alpha: 1.0)
    }
}
