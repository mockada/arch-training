//
//  TravelPackagePresenter.swift
//  arch-training
//
//  Created by Jade Silveira on 02/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import Foundation

class TravelPackagePresenter: NSObject {
    
    var travelPackageInteractor : TravelPackageInteractorInput?
    var userInterface           : TravelPackageInterface?
    
    init(userInterface: TravelPackageInterface) {
        super.init()
        
        self.travelPackageInteractor    = TravelPackageInteractor(output: self)
        self.userInterface              = userInterface
    }
}

extension TravelPackagePresenter: TravelPackageModuleInterface{
    func viewDidLoad() {
        userInterface?.setupTravelPackage()
        userInterface?.setupDescriptionBox()
    }
    
    func viewWillAppear() {
        userInterface?.updateTravelPkg()
    }
    
    func purchaseButtonDidTap(travelPackage: TravelPackage) {
        userInterface?.showMessageConfirmPurchase()
    }
    
    func buyTravelPackage() {
        userInterface?.showMessageBought()
    }
    
    func showPkgAcquired(travelPackage: TravelPackage) {
        travelPackageInteractor?.setTravelPkgBought(travelPackage: travelPackage)
        
        userInterface?.animatePkgAcquired()
        userInterface?.changeToCancel()
    }
}

extension TravelPackagePresenter: TravelPackageInteractorOutput {
    
}
