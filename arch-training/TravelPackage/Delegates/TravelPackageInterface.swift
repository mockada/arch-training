//
//  TravelPackageInterface.swift
//  arch-training
//
//  Created by Jade Silveira on 02/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

protocol TravelPackageInterface {
    func showMessageConfirmPurchase()
    func setupTravelPackage()
    func setupDescriptionBox()
    func updateTravelPkg()
    func showMessageBought()
    func animatePkgAcquired()
    func changeToCancel()
    func changeToBuy()
}

protocol TravelPackageModuleInterface {
    func viewDidLoad()
    func viewWillAppear()
    func buyTravelPackage()
    func showPkgAcquired(travelPackage: TravelPackage)
}
