//
//  CustomUIImageView.swift
//  arch-training
//
//  Created by Jade Silveira on 04/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class CustomUIImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
        setupBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
        setupBorder()
    }
    
    func setupBorder() {
        let borderWidth : CGFloat   = 2.0
        
        self.layer.borderColor      = UIColor.init(red: 232/255.0, green: 113/255.0, blue: 80/255.0, alpha: 1).cgColor
        self.layer.borderWidth      = borderWidth
        self.layer.masksToBounds    = true
    }
    
    func setupRoundCorners() {
        self.layer.cornerRadius     = 10
    }
}
