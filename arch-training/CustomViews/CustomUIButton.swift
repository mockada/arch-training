//
//  CustomUIButton.swift
//  arch-training
//
//  Created by Jade Silveira on 04/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class CustomUIButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
    }
    
    func setupRoundCorners() {
        self.layer.cornerRadius   = 10
    }
}
