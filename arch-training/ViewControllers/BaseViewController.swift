//
//  BaseViewController.swift
//  arch-training
//
//  Created by Jade Silveira on 29/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    func getUIAlertControllerWithTitle(_ title: String, message: String, actionTitle: String, handler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle:.alert)
        
        let alertAction = UIAlertAction(title: actionTitle,
                                        style: .default,
                                        handler: handler)
        
        alertController.addAction(alertAction)
        
        return alertController
    }
    
    func getUIAlertWithTitle(_ title: String, message: String, positiveButtonTitle: String, negativeButtonTitle: String, positiveHandler: ((UIAlertAction) -> Void)?, negativeHandler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alert = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle:.alert)
        
        let negativeAction = UIAlertAction(title: negativeButtonTitle,
                                           style: .default,
                                           handler: negativeHandler)
        alert.addAction(negativeAction)
        
        let positiveAction = UIAlertAction(title: positiveButtonTitle,
                                           style: .default,
                                           handler: positiveHandler)
        alert.addAction(positiveAction)
        
        return alert
    }
}
