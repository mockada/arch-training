//
//  TravelPackageService.swift
//  arch-training
//
//  Created by Jade Silveira on 28/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class TravelPackageService: NSObject {
    
    static let shared       = TravelPackageService()
    
    var travelPackageList   = [TravelPackage]()
    
    func getTravelPackageList() {
        travelPackageList = [TravelPackage]()
        
        if let path = Bundle.main.path(forResource: "travelPackages", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? Array<AnyObject>
                travelPackageList = Mapper<TravelPackage>().mapArray(JSONObject: jsonResult)!
            } catch {
                //handle error
            }
        }
    }
    
    func setTravelPackageBought(travelPackageParam: TravelPackage, wasBought: Bool) {
        if let travelPkgIndex = travelPackageList.index(where: { travelPackage in
            travelPackage.title == travelPackageParam.title
        }) {
            TravelPackageService.shared.travelPackageList[travelPkgIndex].wasBought = wasBought
        }
    }
}
