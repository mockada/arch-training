//
//  TravelPackageListInteractorIO.swift
//  arch-training
//
//  Created by Jade Silveira on 29/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

protocol TravelPackageListInteractorInput {
    func getTravelPackageList()
}

protocol TravelPackageListInteractorOutput {
    func foundTravelPackages()
}
