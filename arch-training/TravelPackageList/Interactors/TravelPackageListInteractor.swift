//
//  TravelPackageListInteractor.swift
//  arch-training
//
//  Created by Jade Silveira on 29/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class TravelPackageListInteractor: NSObject {
    
    var output      : TravelPackageListInteractorOutput?
    
    init(output: TravelPackageListInteractorOutput) {
        super.init()
        
        self.output = output
    }
}

extension TravelPackageListInteractor: TravelPackageListInteractorInput {
    func getTravelPackageList() {
        TravelPackageService.shared.getTravelPackageList()
        output?.foundTravelPackages()
    }
}
