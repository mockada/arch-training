//
//  TravelPackageListInterface.swift
//  arch-training
//
//  Created by Jade Silveira on 29/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

protocol TravelPackageListInterface {
    func updateList()
    func loadTravelPackages()
    func showTravelPackage(_ travelPackage: TravelPackage, segue: UIStoryboardSegue)
    func setupTable()
}

protocol TravelPackageListModuleInterface {
    func viewDidLoad()
    func viewWillAppear()
    func segueWillBePerformed(segue: UIStoryboardSegue, sender: Any?)
}
