//
//  TravelPackageCell.swift
//  arch-training
//
//  Created by Jade Silveira on 27/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class TravelPackageCell: UITableViewCell {
    
    @IBOutlet weak var travelImage  : UIImageView!
    @IBOutlet weak var title        : UILabel!
    @IBOutlet weak var travelPrice  : UILabel!
    
    var travelPackage               = TravelPackage()
    
    func setupCell() {
        title.text          = travelPackage.title
        travelPrice.text    = travelPackage.getPrice()
        travelImage.image   = UIImage(named: travelPackage.image ?? "")
    }
}
