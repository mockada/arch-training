//
//  TravelPackageListViewController.swift
//  arch-training
//
//  Created by Jade Silveira on 27/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

class TravelPackageListViewController: BaseViewController {
    var eventHandler                : TravelPackageListModuleInterface?
    var travelPackages              = [TravelPackage]()
    
    let cellIdentifier              = "TravelPackageCell"
    
    @IBOutlet weak var tableView        : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventHandler = TravelPackageListPresenter(userInterface: self)
        eventHandler?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        eventHandler?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: SEGUE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        eventHandler?.segueWillBePerformed(segue: segue, sender: sender)
    }
}

extension TravelPackageListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelPackages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TravelPackageCell
        cell.travelPackage              = travelPackages[indexPath.row]
        cell.setupCell()
        
        return cell
    }
}

extension TravelPackageListViewController: UITableViewDelegate {
    
}

extension TravelPackageListViewController: TravelPackageListInterface {
    func updateList() {
        loadTravelPackages()
        if !travelPackages.isEmpty { tableView.reloadData() }
    }
    
    func loadTravelPackages() {
        self.travelPackages = TravelPackageService.shared.travelPackageList
    }
    
    func showTravelPackage(_ travelPackage: TravelPackage, segue: UIStoryboardSegue) {
        let travelPackageVC                = segue.destination as! TravelPackageViewController
        travelPackageVC.travelPackage      = travelPackage
    }
    
    func setupTable() {
        self.tableView.separatorStyle = .none
    }
}
