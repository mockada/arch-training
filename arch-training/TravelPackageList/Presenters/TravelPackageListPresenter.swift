//
//  TravelPackageListPresenter.swift
//  arch-training
//
//  Created by Jade Silveira on 29/11/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import Foundation
import UIKit

class TravelPackageListPresenter: NSObject {
    
    var travelPackageInteractor : TravelPackageListInteractorInput?
    var userInterface           : TravelPackageListInterface?
    
    init(userInterface: TravelPackageListInterface) {
        super.init()
        
        self.travelPackageInteractor    = TravelPackageListInteractor(output: self)
        self.userInterface              = userInterface
    }
}

extension TravelPackageListPresenter: TravelPackageListModuleInterface{
    func viewDidLoad() {
        userInterface?.setupTable()
        
        travelPackageInteractor?.getTravelPackageList()
    }
    
    func viewWillAppear() {
        userInterface?.updateList()
    }
    
    func segueWillBePerformed(segue: UIStoryboardSegue, sender: Any?) {
        guard let segueIdentifier = segue.identifier,
            let travelPackageCell = sender as? TravelPackageCell else {
            return
        }
        
        if segueIdentifier == "SegueToTravelPkgVC" {
            userInterface?.showTravelPackage(travelPackageCell.travelPackage, segue: segue)
        }
    }
}

extension TravelPackageListPresenter: TravelPackageListInteractorOutput {
    func foundTravelPackages() {
        userInterface?.loadTravelPackages()
    }
}
