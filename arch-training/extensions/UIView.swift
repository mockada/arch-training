//
//  UIView.swift
//  arch-training
//
//  Created by Jade Silveira on 02/12/18.
//  Copyright © 2018 jadesilveira. All rights reserved.
//

import UIKit

extension UIView {
    func fadeIn(duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, customStartAlpha: CGFloat? = nil, customEndAlpha: CGFloat = 1.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        
        if let customStartAlpha = customStartAlpha {
            self.alpha = customStartAlpha
        }
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden   = false
            self.alpha      = customEndAlpha
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, customStartAlpha: CGFloat? = nil, customEndAlpha: CGFloat = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        
        if let customStartAlpha = customStartAlpha {
            self.alpha = customStartAlpha
        }
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = customEndAlpha
        }, completion: completion)
    }
}
